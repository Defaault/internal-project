package com.epam.ipatov.artem.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MessageDigest.class, PasswordEncoderUtil.class})
public class PasswordEncoderUtilTest {

    @Test
    public void matchesShouldReturnTrueWhenPasswordsAreEquals() {
        boolean result = PasswordEncoderUtil.matches("qwe", PasswordEncoderUtil.encodePassword("qwe"));

        assertTrue(result);
    }

    @Test
    public void matchesShouldReturnFalseWhenPasswordsAreNotEquals() {
        boolean result = PasswordEncoderUtil.matches("qwwe", PasswordEncoderUtil.encodePassword("qwe"));

        assertFalse(result);
    }

    @Test
    public void matchesShouldThrowNoSuchAlgorithmException() throws NoSuchAlgorithmException {
        mockStatic(MessageDigest.class);
        Mockito.when(MessageDigest.getInstance("SHA-256")).thenThrow(NoSuchAlgorithmException.class);
        boolean result = PasswordEncoderUtil.matches("qwe", PasswordEncoderUtil.encodePassword("qwe"));

        assertFalse(result);
    }

    @Test
    public void encodePasswordShouldReturnString() {
        Objects.requireNonNull(PasswordEncoderUtil.encodePassword("qwe"));
        assertTrue(Objects.requireNonNull(PasswordEncoderUtil.encodePassword("qwe")).length() > 0);
    }

    @Test
    public void encodePasswordShouldReturnNullWhenThrowNoSuchAlgorithmException() throws NoSuchAlgorithmException {
        mockStatic(MessageDigest.class);
        Mockito.when(MessageDigest.getInstance("SHA-256")).thenThrow(NoSuchAlgorithmException.class);

        String result = PasswordEncoderUtil.encodePassword("qwe");

        assertNull(result);
    }
}
