package com.epam.ipatov.artem.provider;

import com.epam.ipatov.artem.provider.impl.CookieKeyProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static com.epam.ipatov.artem.constant.Constant.ID;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CookieKeyProviderTest {

    @Mock
    private HttpServletRequest request;

    @Test
    public void getShouldReturnNullWhenRequestedCookieNotExist() {
        CookieKeyProvider cookieKeyProvider = new CookieKeyProvider();
        Cookie[] cookies = new Cookie[1];
        cookies[0] = new Cookie("qwe", "qwe");
        when(request.getCookies()).thenReturn(cookies);

        assertNull(cookieKeyProvider.get(request));
    }

    @Test
    public void getShouldReturnCookieWhenRequestedCookieExist() {
        CookieKeyProvider cookieKeyProvider = new CookieKeyProvider();
        Cookie[] cookies = new Cookie[1];
        cookies[0] = new Cookie(ID, "qwe");
        when(request.getCookies()).thenReturn(cookies);

        assertNotNull(cookieKeyProvider.get(request));
    }
}
