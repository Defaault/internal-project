package com.epam.ipatov.artem.servlet;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.service.UserService;
import com.epam.ipatov.artem.util.PasswordEncoderUtil;
import com.epam.ipatov.artem.validate.UserValidate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.INDEX_JSP;
import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.LOGIN_URL;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.USER_SERVICE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserValidate.class)
public class LoginServletTest {

    @Mock
    private HttpSession session;

    @Mock
    private UserService userService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private ServletConfig config;

    @Mock
    private ServletContext context;

    @Test
    public void doPostShouldAddUserDoesNotExistWhenUserIsNull() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> errors = new HashMap<>();
        LoginServlet loginServlet = new LoginServlet();
        when(request.getSession()).thenReturn(session);
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(UserValidate.validate(any())).thenReturn(errors);
        when(userService.getUser(any())).thenReturn(null);
        loginServlet.init(config);

        loginServlet.doPost(request, response);

        assertEquals("User does not exist", errors.get(LOGIN));
        assertEquals(1, errors.size());
        verify(response).sendRedirect(LOGIN_URL);
    }

    @Test
    public void doPostShouldAddPasswordDoNotMatchWhenPasswordNotMatch() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> errors = new HashMap<>();
        LoginServlet loginServlet = new LoginServlet();
        when(request.getSession()).thenReturn(session);
        when(request.getParameter(PASSWORD)).thenReturn("qweewq");
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(UserValidate.validate(any())).thenReturn(errors);
        User user = User.builder().password("qweewq").build();
        when(userService.getUser(any())).thenReturn(user);
        loginServlet.init(config);

        loginServlet.doPost(request, response);

        assertEquals("Passwords do not match", errors.get(PASSWORD));
        assertEquals(1, errors.size());
        verify(response).sendRedirect(LOGIN_URL);
    }

    @Test
    public void doPostShouldAddValidationErrorsWhenSomeFieldIncorrect() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> errors = new HashMap<>();
        errors.put(LOGIN, "qwe");
        errors.put(PASSWORD, "qwe");
        LoginServlet loginServlet = new LoginServlet();
        when(request.getSession()).thenReturn(session);
        when(request.getParameter(PASSWORD)).thenReturn("qweewq");
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(UserValidate.validate(any())).thenReturn(errors);
        User user = User.builder().password(PasswordEncoderUtil.encodePassword("qweewq")).build();
        when(userService.getUser(any())).thenReturn(user);
        loginServlet.init(config);

        loginServlet.doPost(request, response);

        assertNotEquals("User does not exist", errors.get(LOGIN));
        assertNotEquals("Passwords do not match", errors.get(PASSWORD));
        assertEquals(2, errors.size());
        verify(response).sendRedirect(LOGIN_URL);
    }

    @Test
    public void doPostShouldPassValidationWhenFieldsCorrect() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> errors = new HashMap<>();
        LoginServlet loginServlet = new LoginServlet();
        when(request.getSession()).thenReturn(session);
        when(request.getParameter(PASSWORD)).thenReturn("qweewq");
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(UserValidate.validate(any())).thenReturn(errors);
        User user = User.builder().password(PasswordEncoderUtil.encodePassword("qweewq")).build();
        when(userService.getUser(any())).thenReturn(user);
        loginServlet.init(config);

        loginServlet.doPost(request, response);

        assertEquals(0, errors.size());
        verify(response).sendRedirect(INDEX_JSP);
    }
}
