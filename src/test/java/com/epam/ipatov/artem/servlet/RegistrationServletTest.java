package com.epam.ipatov.artem.servlet;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.repository.impl.UserRepositoryImpl;
import com.epam.ipatov.artem.service.UserService;
import com.epam.ipatov.artem.service.impl.FileUploadServiceImpl;
import com.epam.ipatov.artem.service.impl.UserServiceImpl;
import com.epam.ipatov.artem.storage.CaptchaStorage;
import com.epam.ipatov.artem.validate.UserValidate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_STORAGE;
import static com.epam.ipatov.artem.constant.Constant.ERRORS;
import static com.epam.ipatov.artem.constant.Constant.FILE_UPLOAD_SERVICE;
import static com.epam.ipatov.artem.constant.Constant.INDEX_JSP;
import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.REGISTER_URL;
import static com.epam.ipatov.artem.constant.Constant.USER_SERVICE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserValidate.class)
public class RegistrationServletTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private ServletConfig config;

    @Mock
    private ServletContext context;

    @Mock
    private CaptchaStorage storage;

    @Mock
    private HttpSession session;

    @Mock
    private FileUploadServiceImpl uploadService;

    @Test
    public void doPostShouldAddErrorUserAlreadyExistWhenUserExistInRepository() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> map = new HashMap<>();
        RegistrationServlet servlet = new RegistrationServlet();
        UserService userService = new UserServiceImpl(new UserRepositoryImpl());
        User user = User.builder()
                .login("test").build();
        userService.create(user);
        when(UserValidate.validate(any(), any())).thenReturn(map);
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(context.getAttribute(CAPTCHA_STORAGE)).thenReturn(storage);
        when(request.getParameter(LOGIN)).thenReturn("test");
        when(request.getSession()).thenReturn(session);
        servlet.init(config);

        servlet.doPost(request, response);

        assertEquals("User with provided login already exist", map.get(LOGIN));
        verify(session).setAttribute(ERRORS, map);
        verify(response).sendRedirect(REGISTER_URL);
        verify(response, never()).sendRedirect(INDEX_JSP);
    }

    @Test
    public void doPostShouldNotAddErrorUserAlreadyExistWhenUserNotExistInRepository() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> map = new HashMap<>();
        RegistrationServlet servlet = new RegistrationServlet();
        UserService userService = new UserServiceImpl(new UserRepositoryImpl());
        User user = User.builder()
                .login("test")
                .build();
        userService.create(user);
        when(UserValidate.validate(any(), any())).thenReturn(map);
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(context.getAttribute(FILE_UPLOAD_SERVICE)).thenReturn(uploadService);
        when(context.getAttribute(CAPTCHA_STORAGE)).thenReturn(storage);
        when(request.getParameter(LOGIN)).thenReturn("check");
        when(request.getParameter(PASSWORD)).thenReturn("qwe");
        when(request.getSession()).thenReturn(session);
        servlet.init(config);

        servlet.doPost(request, response);

        assertEquals(0, map.size());
        verify(session, never()).setAttribute(ERRORS, map);
        verify(response, never()).sendRedirect(REGISTER_URL);
        verify(response).sendRedirect(INDEX_JSP);
    }

    @Test
    public void doPostShouldAddValidationToSessionErrorsWhenUserUnique() throws ServletException, IOException {
        mockStatic(UserValidate.class);
        Map<String, String> map = new HashMap<>();
        map.put("someError", "error");
        RegistrationServlet servlet = new RegistrationServlet();
        UserService userService = new UserServiceImpl(new UserRepositoryImpl());
        User user = User.builder()
                .login("test").build();
        userService.create(user);
        when(UserValidate.validate(any(), any())).thenReturn(map);
        when(config.getServletContext()).thenReturn(context);
        when(context.getAttribute(USER_SERVICE)).thenReturn(userService);
        when(context.getAttribute(CAPTCHA_STORAGE)).thenReturn(storage);
        when(request.getParameter(LOGIN)).thenReturn("check");
        when(request.getSession()).thenReturn(session);
        servlet.init(config);

        servlet.doPost(request, response);

        assertNull(map.get(LOGIN));
        verify(session).setAttribute(ERRORS, map);
        verify(response).sendRedirect(REGISTER_URL);
        verify(response, never()).sendRedirect(INDEX_JSP);
    }
}
