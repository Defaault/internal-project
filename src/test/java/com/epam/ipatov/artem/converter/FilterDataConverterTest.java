package com.epam.ipatov.artem.converter;

import com.epam.ipatov.artem.entity.dto.FilterData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static com.epam.ipatov.artem.constant.Constant.CATEGORY;
import static com.epam.ipatov.artem.constant.Constant.MANUFACTURER;
import static com.epam.ipatov.artem.constant.Constant.NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FilterDataConverterTest {

    private static final String PAGE = "page";
    private static final String MIN_PRICE = "minPrice";
    private static final String MAX_PRICE = "maxPrice";
    private static final String COUNT = "count";
    private static final String ORDER_BY = "orderBy";

    @Mock
    private HttpServletRequest request;

    @Test
    public void createDataShouldReturnFullFilledFilterDataWhenValuesNotNull() {
        FilterDataConverter filterDataConverter = new FilterDataConverter();
        when(request.getParameter(NAME)).thenReturn("qwe");
        when(request.getParameterValues(MANUFACTURER)).thenReturn(new String[]{"qwe"});
        when(request.getParameterValues(CATEGORY)).thenReturn(new String[]{"qwe"});
        when(request.getParameter(MIN_PRICE)).thenReturn("123");
        when(request.getParameter(MAX_PRICE)).thenReturn("124");
        when(request.getParameter(COUNT)).thenReturn("5");
        when(request.getParameter(ORDER_BY)).thenReturn("qwe");
        when(request.getParameter(PAGE)).thenReturn("5");

        FilterData data = filterDataConverter.createData(request);

        assertEquals("qwe", data.getName());
        assertEquals(5, data.getPage());
        assertEquals(123, data.getMinPrice());
        assertEquals(124, data.getMaxPrice());
        assertEquals(5, data.getCount());
        assertEquals("qwe", data.getOrderBy());
        assertEquals(1, data.getManufacturer().size());
        assertEquals("qwe", data.getManufacturer().get(0));
        assertEquals(1, data.getCategory().size());
        assertEquals("qwe", data.getCategory().get(0));
    }

    @Test
    public void createDataShouldReturnEmptyFilterDataWhenValuesNull() {
        FilterDataConverter filterDataConverter = new FilterDataConverter();
        when(request.getParameter(COUNT)).thenReturn("qwe");

        FilterData data = filterDataConverter.createData(request);

        assertNull(data.getName());
        assertEquals(1, data.getPage());
        assertEquals(0, data.getMinPrice());
        assertEquals(Integer.MAX_VALUE, data.getMaxPrice());
        assertEquals(5, data.getCount());
        assertNull(data.getOrderBy());
        assertNull(data.getManufacturer());
        assertNull(data.getCategory());
    }

    @Test
    public void createDataShouldReturnMaxPriceEqualsMinWhenItLowerThanMin() {
        FilterDataConverter filterDataConverter = new FilterDataConverter();
        when(request.getParameter(COUNT)).thenReturn("qwe");
        when(request.getParameter(MIN_PRICE)).thenReturn("124");
        when(request.getParameter(MAX_PRICE)).thenReturn("123");

        FilterData data = filterDataConverter.createData(request);

        assertEquals(data.getMinPrice(), data.getMaxPrice());
    }
}
