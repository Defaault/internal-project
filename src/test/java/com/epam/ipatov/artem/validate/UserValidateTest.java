package com.epam.ipatov.artem.validate;

import com.epam.ipatov.artem.entity.dto.CheckinUser;
import com.epam.ipatov.artem.entity.dto.LoginUser;
import ml.miron.captcha.image.Captcha;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_INPUT;
import static com.epam.ipatov.artem.constant.Constant.EMAIL;
import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.NAME;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.REPEAT_PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.SURNAME;
import static com.epam.ipatov.artem.validate.UserValidate.validate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class UserValidateTest {

    @Test
    public void validateShouldAddLoginErrorWhenLoginLengthLessThanThree() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("q")
                .name("qwee")
                .surname("qweqw")
                .email("qwe@qwe.qwe")
                .password("qweqweqwe")
                .repeatPassword("qweqweqwe")
                .captcha(captcha.getAnswer())
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(LOGIN));
    }

    @Test
    public void validateShouldAddLoginErrorWhenEmailNotRight() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("qqweqwe")
                .name("qwee")
                .surname("qweqw")
                .email("qwe@q.qwe.$%%$we.qwe")
                .password("qweqweqwe")
                .repeatPassword("qweqweqwe")
                .captcha(captcha.getAnswer())
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(EMAIL));
    }

    @Test
    public void validateShouldAddLoginErrorWhenPasswordLessThanEight() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("qqweqwe")
                .name("qwee")
                .surname("qweqw")
                .email("qwe@qwe.qwe")
                .password("qweqweq")
                .repeatPassword("qweqweq")
                .captcha(captcha.getAnswer())
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(2, result.size());
        assertTrue(result.containsKey(PASSWORD));
        assertEquals("The length of the confirm password must be from 8 to 26 Latin characters or numbers.", result.get(REPEAT_PASSWORD));
    }

    @Test
    public void validateShouldAddLoginErrorWhenRepeatPasswordNotMatch() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("qqweqwe")
                .name("qwee")
                .surname("qweqw")
                .email("qwe@qwe.qwe")
                .password("qweqweqwe")
                .repeatPassword("qweqweqweqwe")
                .captcha(captcha.getAnswer())
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(REPEAT_PASSWORD));
        assertEquals("Passwords do not match.", result.get(REPEAT_PASSWORD));
    }

    @Test
    public void validateShouldAddLoginErrorWhenNameLengthLessThanThree() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("qqweqwe")
                .name("qw")
                .surname("qweqw")
                .email("qwe@qwe.qwe")
                .password("qweqweqwe")
                .repeatPassword("qweqweqwe")
                .captcha(captcha.getAnswer())
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(NAME));
    }

    @Test
    public void validateShouldAddLoginErrorWhenSurnameLengthLessThanThree() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("qqweqwe")
                .name("qwqweqe")
                .surname("qw")
                .email("qwe@qwe.qwe")
                .password("qweqweqwe")
                .repeatPassword("qweqweqwe")
                .captcha(captcha.getAnswer())
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(SURNAME));
    }

    @Test
    public void validateShouldAddLoginErrorWhenCaptchaAnswerNotCorrect() {
        Captcha captcha = new Captcha.Builder(100, 100).build();
        CheckinUser checkinUser = CheckinUser.builder()
                .login("qqweqwe")
                .name("qqwewqw")
                .surname("qweqw")
                .email("qwe@qwe.qwe")
                .password("qweqweqwe")
                .repeatPassword("qweqweqwe")
                .captcha("qwe")
                .build();

        Map<String, String> result = validate(checkinUser, captcha);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(CAPTCHA_INPUT));
    }

    @Test
    public void validateShouldAddLoginErrorWhenLoginLessThanThree() {
        LoginUser user = LoginUser.builder()
                .login("")
                .password("qweqweqweqwe")
                .build();

        Map<String, String> result = validate(user);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(LOGIN));
    }

    @Test
    public void validateShouldAddPasswordErrorWhenPasswordLessThanEight() {
        LoginUser user = LoginUser.builder()
                .login("qwewqewqeqe")
                .password("eqwqwe")
                .build();

        Map<String, String> result = validate(user);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(PASSWORD));
    }
}
