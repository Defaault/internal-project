package com.epam.ipatov.artem.service;

import com.epam.ipatov.artem.service.impl.FileUploadServiceImpl;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

import static com.epam.ipatov.artem.constant.Constant.OCTET_STREAM;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileUploadServiceImplTest {

    private static final String AVATAR_PATH_TEST = "src/test/resources/avatars";

    @Mock
    private Part part;

    @After
    public void afterComplete() {
        File file = new File(AVATAR_PATH_TEST);

        if (file.exists()) {
            file.delete();
        }
    }

    @Test
    public void uploadShouldReturnFileNameNullWhenPartIsNull() {
        FileUploadServiceImpl uploadService = new FileUploadServiceImpl();
        File file = new File(AVATAR_PATH_TEST);

        String result = uploadService.upload(null, "", AVATAR_PATH_TEST);

        assertNull(result);
        assertFalse(file.exists());
    }

    @Test
    public void uploadShouldReturnFileNameWhenPartNotNull() {
        FileUploadServiceImpl uploadService = new FileUploadServiceImpl();
        when(part.getContentType()).thenReturn("qwe");
        File file = new File(AVATAR_PATH_TEST);

        String result = uploadService.upload(part, "qwe", AVATAR_PATH_TEST);

        assertEquals("qwe.qwe", result);
        assertTrue(file.exists());
    }

    @Test
    public void uploadShouldReturnFileNameNullWhenThrowIOException() throws IOException {
        FileUploadServiceImpl uploadService = new FileUploadServiceImpl();
        when(part.getContentType()).thenReturn("qwe/qwe");
        doThrow(IOException.class).when(part).write(any());
        File file = new File(AVATAR_PATH_TEST);

        String result = uploadService.upload(part, "qwe", AVATAR_PATH_TEST);

        assertNull(result);
        assertTrue(file.exists());
    }

    @Test
    public void uploadShouldReturnNullWhenExtensionEqualsOctetStream() {
        FileUploadServiceImpl uploadService = new FileUploadServiceImpl();
        when(part.getContentType()).thenReturn(OCTET_STREAM);
        File file = new File(AVATAR_PATH_TEST);

        String result = uploadService.upload(part, "qwe", AVATAR_PATH_TEST);

        assertNull(result);
        assertTrue(file.exists());
    }
}
