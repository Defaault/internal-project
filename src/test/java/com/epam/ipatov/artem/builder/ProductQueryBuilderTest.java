package com.epam.ipatov.artem.builder;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProductQueryBuilderTest {

    @Test
    public void nameShouldAddPredicateWhenNameNotNull() {
        String result = ProductQueryBuilder.builder().name("qwe").build();

        assertTrue(result.contains("name like '%qwe%'"));
    }

    @Test
    public void nameShouldNotAddPredicateWhenNameIsNull() {
        String result = ProductQueryBuilder.builder().name(null).build();

        assertFalse(result.contains("name like"));
    }

    @Test
    public void categoryShouldAddPredicateWhenCategoryNotNull() {
        String result = ProductQueryBuilder.builder().category(Collections.singletonList("qwe")).build();

        assertTrue(result.contains("category in ('qwe')"));
    }

    @Test
    public void categoryShouldNotAddPredicateWhenCategoryIsNull() {
        String result = ProductQueryBuilder.builder().name(null).build();

        assertFalse(result.contains("category in"));
    }

    @Test
    public void manufacturerShouldAddPredicateWhenManufacturerNotNull() {
        String result = ProductQueryBuilder.builder().manufacturer(Collections.singletonList("qwe")).build();

        assertTrue(result.contains("manufacturer in ('qwe')"));
    }

    @Test
    public void manufacturerShouldNotAddPredicateWhenManufacturerIsNull() {
        String result = ProductQueryBuilder.builder().manufacturer(null).build();

        assertFalse(result.contains("manufacturer in"));
    }

    @Test
    public void orderShouldAddPredicateWhenOrderNotNull() {
        String result = ProductQueryBuilder.builder().name("ewq").order("qwe").build();

        assertTrue(result.contains("order by qwe"));
    }

    @Test
    public void orderShouldNotAddPredicateWhenOrderIsNull() {
        String result = ProductQueryBuilder.builder().name("qwe").order(null).build();

        assertFalse(result.contains("order by "));
    }

    @Test
    public void buildShouldNotAddOrderByWhenItNotNull() {
        String result = ProductQueryBuilder.builder().name("qwe").order("qwe").build();

        assertTrue(result.contains("order by "));
    }

    @Test
    public void buildShouldAddOrderByWhenItIsNull() {
        String result = ProductQueryBuilder.builder().name("qwe").order(null).build();

        assertFalse(result.contains("order by "));
    }

    @Test
    public void buildShouldAddPredicatesWhenItSizeBiggerThanZero() {
        String result = ProductQueryBuilder.builder().name("qwe").build();

        assertTrue(result.contains("where"));
    }

    @Test
    public void buildShouldNotAddPredicatesWhenItSizeIsZero() {
        String result = ProductQueryBuilder.builder().build();

        assertFalse(result.contains("where"));
    }
}
