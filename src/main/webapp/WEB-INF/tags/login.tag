<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-5">
	<div class="basic-login">
		<form id="loginForm" role="form" action="/login" method="post">
			<div class="form-group">
            	<label for="login"><i class="icon-user"></i> <b>Login</b></label>
                <c:choose>
                    <c:when test = "${not empty loginForm.login}">
                        <input class="form-control" name="login" id="login" value="${loginForm.login}" type="text" placeholder="">
                    </c:when>
                    <c:otherwise>
                        <input class="form-control" name="login" id="login" type="text" placeholder="">
                    </c:otherwise>
                </c:choose>
			</div>
			<div class="form-group">
				<label for="password"><i class="icon-lock"></i> <b>Password</b></label>
				<input class="form-control" name="password" id="password" type="password" placeholder="">
			</div>
			<div class="form-group">
				<label class="checkbox">
				    <input type="checkbox"> Remember me
				</label>
				<a href="page-password-reset.html" class="forgot-password">Forgot password?</a>
				<button type="submit" class="btn pull-right">Login</button>
			    <div class="clearfix"></div>
	        </div>
		</form>
	</div>
</div>