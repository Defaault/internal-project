<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<c:choose>
    <c:when test = "${not empty user}">
        <li>
            <form action="/logout" method="post">
                ${user.name} ${user.surname}
                <c:if test="${not empty user.picture}">
			        <img id="avatar" src="/avatar">
                </c:if>
                <button id="logout_link" type="submit" onclick="">Logout</button>
            </form>
        </li>
    </c:when>
    <c:otherwise>
        <li><a href="/login">Login</a></li>
    </c:otherwise>
</c:choose>