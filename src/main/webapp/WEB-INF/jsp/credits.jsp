<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>

<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf" %>

	<!-- Page Title -->
	<div class="section section-breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Credits</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="container">
			<h2>Many Thanks To</h2>
			<div class="row">
				<div class="col-md-12">
					<ul>
						<li><a href="http://www.morguefile.com">morgueFile</a> - for photos</li>
						<li><a href="http://www.flickr.com/creativecommons">Flickr Creative Common</a> - for photos</li>
						<li><a href="http://fotogrph.com">fotogrph.com</a> - for photos</li>
						<li><a href="http://www.premiumpixels.com">premiumpixels.com</a> - for free PSDs</li>
						<li><a href="http://365psd.com">365psd.com</a> - for free PSDs</li>
						<li><a href="http://icomoon.io/app">icomoon.io</a> - for Icons</li>
						<li><a href="http://designmoo.com/members/liammckay">Liam McKay</a> - for Browser &amp; Magnify
							PSD</li>
						<li><a href="http://www.pixeden.com">pixeden.com</a> - for Responsive Swhowcase PSD</li>
						<li><a href="http://jquery.com">jQuery</a> - great JavaScript library</li>
						<li><a href="http://bxslider.com">bxSlider</a> - nice content slider plugin</li>
						<li><a href="http://www.sequencejs.com">sequencejs</a> - awsome slider with CSS3 transitions
						</li>
						<li><a href="http://lesscss.org">LESS</a> - dynamic stylesheet language</li>
						<li><a href="http://lesshat.com">lesshat</a> - universal LESS mixin library</li>
						<li><a href="http://www.famfamfam.com">famfamfam.com</a> - for flag icons</li>
						<li><a href="http://tympanus.net/codrops">codrops</a> - for Mega Dropdown Menu</li>
						<li><a href="http://dribbble.com/Ahmedeabbas">Ahmedeabbas</a> - for Polygon Backgrounds</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	<%@ include file="/WEB-INF/jspf/script.jspf" %>
</body>

</html>