<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="l" %>

<html class="no-js">
<!--<![endif]-->

<head>
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>

<body onload="serverValidate('${errors}')">
	<%@ include file="/WEB-INF/jspf/menu.jspf" %>

	<!-- Page Title -->
	<div class="section section-breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Login</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="container">
			<div class="row">
				<l:login/>
				<div class="col-sm-7 social-login">
					<p>Or login with your Facebook or Twitter</p>
					<div class="social-login-buttons">
						<a href="#" class="btn-facebook-login">Login with Facebook</a>
						<a href="#" class="btn-twitter-login">Login with Twitter</a>
					</div>
					<div class="clearfix"></div>
					<div class="not-member">
						<p>Not a member? <a href="/register">Register here</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="js/createError.js"></script>
	<script src="js/jqueryLoginValidate.js"></script>
	<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	<%@ include file="/WEB-INF/jspf/script.jspf" %>
</body>

</html>