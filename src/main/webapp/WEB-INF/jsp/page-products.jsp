<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<html class="no-js">

<head>
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>

<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf" %>

	<!-- Page Title -->
	<div class="section section-breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Drinks
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<form action="/drinks" method="get">
			<table class="shop-item-selections">
				<tr>
					<td class="custom-td">
						<b>Sort by:</b>
						<select class="form-control" name="orderBy">
							<c:forEach items="${applicationScope['ordersBy']}" var="o">
								<c:choose>
									<c:when test="${param.orderBy eq o.name}">
										<option role="menuitem" value="${o.name}" selected><span class="color-white"></span>${o.name}</option>
									</c:when>
									<c:otherwise>
										<option role="menuitem" value="${o.name}"><span class="color-white"></span>${o.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>						
						</select>
					</td>
					<td class="custom-td">
						<b>Category:</b>
						<select class="selectpicker" data-selected-text-format="count" data-width="auto" multiple name="category">
							<c:forEach items="${requestScope.categories}" var="c">
								<c:choose>
									<c:when test="${fn:contains(requestScope.categoriesList, c)}">
										<option role="menuitem" value="${c}" selected><span class="color-white"></span>${c}</option>
									</c:when>
									<c:otherwise>
										<option role="menuitem" value="${c}"><span class="color-white"></span>${c}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</td>
					<td class="custom-td">
						<b>Manufacturer:</b>
						<select class="selectpicker" data-selected-text-format="count" data-width="auto" multiple name="manufacturer">
							<c:forEach items="${requestScope.manufactures}" var="m">
								<c:choose>
									<c:when test="${fn:contains(requestScope.manufacturerList, m)}">
										<option role="menuitem" value="${m}" selected><span class="color-white"></span>${m}</option>
									</c:when>
									<c:otherwise>
										<option role="menuitem" value="${m}"><span class="color-white"></span>${m}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</td>
					<td class="custom-td">
						<b>Count:</b>
						<select class="form-control" name="count">
							<c:forEach items="${applicationScope['pagination']}" var="p">
								<c:choose>
									<c:when test="${param.count eq 'p.count'}">
										<option role="menuitem" value="${p.count}" selected><span class="color-white"></span>${p.count}</option>
									</c:when>
									<c:otherwise>
										<option role="menuitem" value="${p.count}"><span class="color-white"></span>${p.count}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
					<td class="custom-td">
						<b>Name:</b>
						<c:choose>
							<c:when test="${not empty param.name}">
								<input type="text" name="name" class="form-control input-sm input-custom" value="${param.name}">
							</c:when>
							<c:otherwise>
								<input type="text" name="name" placeholder="Artem" class="form-control input-sm input-custom">
							</c:otherwise>
						</c:choose>									
					</td>
					<td class="custom-td">
						<b>Min Price:</b>
						<c:choose>
							<c:when test="${not empty param.minPrice}">
								<input type="text" name="minPrice" class="form-control input-sm input-custom" value="${param.minPrice}">
							</c:when>
							<c:otherwise>
								<input type="text" name="minPrice" placeholder="0" class="form-control input-sm input-custom" >
							</c:otherwise>
						</c:choose>										
					</td>
					<td class="custom-td">
						<b>Max Price:</b>
						<c:choose>
							<c:when test="${not empty param.maxPrice}">
								<input type="text" name="maxPrice" class="form-control input-sm input-custom" value="${param.maxPrice}">
							</c:when>
							<c:otherwise>
								<input type="text" name="maxPrice" placeholder="0" class="form-control input-sm input-custom">
							</c:otherwise>
						</c:choose>	
					</td>
					<td>&nbsp;</td>
					<td>
						<button type="submit" class="btn pull-right" onclick="">Filter</button>
					</td>
				</tr>
			</table>
		</form>
	</div>

	<div class="eshop-section section">
		<div class="container">
			<div class="row">
				<c:choose>
					<c:when test="${not empty products}">
						<c:forEach items="${products}" var="product">
							<div class="col-md-3 col-sm-6 col-lg-2">
								<!-- Product -->
								<div class="shop-item">
									<!-- Product Title -->
									<div class="title">
										<h3><a href="page-product-details.html">${product.name}</a></h3>
									</div>
									<!-- Product Price-->
									<div class="price">
										${product.price}
									</div>
									<!-- Add to Cart Button -->
									<div class="actions">
										<a href="page-product-details.html" class="btn btn-small"><i
												class="icon-shopping-cart icon-white"></i> Add to Cart</a>
									</div>
								</div>
								<!-- End Product -->
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						Products not found
					</c:otherwise>
				</c:choose>	
			</div>
			<c:if test="${countOfPages ne 1}">
				<div class="pagination-wrapper ">
					<ul class="pagination pagination-lg">
						<c:forEach var = "i" begin = "1" end = "${countOfPages}">
							<c:choose>
								<c:when test="${param.page eq i or (empty param.page and i eq 1)}">
									<li class="active"><a>${i}</a></li>
								</c:when>
								<c:otherwise>
									<li>
									    <c:set var="pagePattern" value="page=${param.page}&"/>
									    <a href="/drinks?page=${i}&${fn:replace(pageContext.request.queryString,pagePattern,'')}">${i}</a>
									</li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</div>
			</c:if>
		</div>
	</div>

	<div class="eshop-section section">
		<div class="container">
			<h2>Listing With No Colors Information</h2>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product1.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product2.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product3.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product4.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product5.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product6.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product7.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="shop-item">
						<div class="shop-item-image">
							<a href="page-product-details.html"><img src="img/product8.jpg" alt="Item Name"></a>
						</div>
						<div class="title">
							<h3><a href="page-product-details.html">Lorem ipsum dolor</a></h3>
						</div>
						<div class="price">
							$999.99
						</div>
						<div class="actions">
							<a href="page-product-details.html" class="btn btn-small"><i
									class="icon-shopping-cart icon-white"></i> Add</a> <span>or <a
									href="page-product-details.html">Read more</a></span>
						</div>
					</div>
				</div>
			</div>
			<div class="pagination-wrapper ">
				<ul class="pagination pagination-lg">
					<li class="disabled"><a href="#">Previous</a></li>
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
					<li><a href="#">Next</a></li>
				</ul>
			</div>
		</div>
	</div>

	<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	<%@ include file="/WEB-INF/jspf/script.jspf" %>
	<script src="js/multipleSelect.js"></script>
</body>

</html>