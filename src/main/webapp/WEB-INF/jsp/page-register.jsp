<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="ctg" uri="captcha" %>

<html class="no-js">
<!--<![endif]-->

<head>
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
</head>(

<body onload="serverValidate('${errors}')">
	<%@ include file="/WEB-INF/jspf/menu.jspf" %>

	<!-- Page Title -->
	<div class="section section-breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Register</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<div class="basic-login">
						<form id="first_form" role="form" action="/register" method="post" enctype="multipart/form-data">
							<div id="pictureDiv" class="form-group">
								<label for="picture"><i class="icon-lock"></i> <b>Choose avatar: </b></label>
								<input class="form-control" name="picture" id="picture" type="file" placeholder="" accept="image/jpeg,image/png">
							</div>
							<div id="loginDiv" class="form-group">
								<label for="login"><i class="icon-user"></i> <b>Login</b></label>
								<c:choose>
									<c:when test = "${not empty form.login}">
										<input id="login" name="login" value="${form.login}"class="form-control" type="text" placeholder="">	
									</c:when>
									<c:otherwise>
										<input id="login" name="login" class="form-control" type="text" placeholder="">		
									</c:otherwise>
								</c:choose>
							</div>
							<div id="nameDiv" class="form-group">
								<label for="name"><i class="icon-user"></i> <b>Name</b></label>
								<c:choose>
									<c:when test = "${not empty form.name}">
										<input id="name" name="name" value="${form.name}"class="form-control" type="text" placeholder="">	
									</c:when>
									<c:otherwise>
										<input id="name" name="name" class="form-control" type="text" placeholder="">		
									</c:otherwise>
								</c:choose>
							</div>
							<div id="surnameDiv" class="form-group">
								<label for="surname"><i class="icon-user"></i> <b>Surname</b></label>
								<c:choose>
									<c:when test = "${not empty form.surname}">
										<input id="surname" name="surname" value="${form.surname}" class="form-control" type="text" placeholder="">	
									</c:when>
									<c:otherwise>
										<input id="surname" name="surname" class="form-control" type="text" placeholder="">		
									</c:otherwise>
								</c:choose>
							</div>
							<div id="emailDiv" class="form-group">
								<label for="email"><i class="icon-user"></i> <b>Email</b></label>
								<c:choose>
									<c:when test = "${not empty form.email}">
										<input id="email" name="email" value="${form.email}" class="form-control" type="text" placeholder="">	
									</c:when>
									<c:otherwise>
										<input id="email" name="email" class="form-control" type="text" placeholder="">	
									</c:otherwise>
								</c:choose>
							</div>
							<div id="passwordDiv" class="form-group">
								<label for="password"><i class="icon-lock"></i> <b>Password</b></label>
								<input class="form-control" name="password" id="password" type="password" placeholder="">
							</div>
							<div id="repeatPasswordDiv" class="form-group">
								<label for="repeat_password"><i class="icon-lock"></i> <b>Re-enter Password</b></label>
								<input class="form-control" name="repeat_password" id="repeat_password" type="password" placeholder="">
							</div>
							<div id="spamDiv" class="form-group">
								<c:choose>
									<c:when test = "${form.spam}">
										<input id="spam" name="spam" type="checkbox" placeholder="" checked>
									</c:when>
									<c:otherwise>
										<input id="spam" name="spam" type="checkbox" placeholder="">
									</c:otherwise>
								</c:choose>
								<label for="spam"><i class="icon-lock"></i> <b>Would you like get mailing?</b></label>
							</div>
							<ctg:captcha/>
							<div class="form-group">
								<button type="submit" class="btn pull-right" onclick="">Register</button>
								<div class="clearfix"></div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-sm-6 col-sm-offset-1 social-login">
					<p>You can use your Facebook or Twitter for registration</p>
					<div class="social-login-buttons">
						<a href="#" class="btn-facebook-login">Use Facebook</a>
						<a href="#" class="btn-twitter-login">Use Twitter</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	<script src="js/createError.js"></script>
	<script src="js/jqueryValidate.js"></script>
	<!--	<script src="js/validate.js"></script>-->
	<%@ include file="/WEB-INF/jspf/script.jspf" %>

</body>

</html>