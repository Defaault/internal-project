$(document).ready(function () {

  $('#first_form').submit(function (e) {
    var login = $('#login').val();
    var name = $('#name').val();
    var surname = $('#surname').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var repeatPassword = $('#repeat_password').val();
    var nameRegex = /^\p{L}{3,20}$/u;
    var passwordRegex = /^\w{8,26}$/;
    var regEx = /^\w{0,63}@(?:\w{1,63}\.){1,125}[a-z]{2,63}$/;
    var status = false;

    $(".error").remove();
    
    if (login.length < 1) {
      $('#login').after('<span class="error">This field is required</span>');
      status = true;
    } else if (!nameRegex.test(login)) {
      $('#login').after('<span class="error">Enter a valid login</span>');
      status = true;
    }
    if (name.length < 1) {
      $('#name').after('<span class="error">This field is required</span>');
      status = true;
    } else if (!nameRegex.test(name)) {
      $('#name').after('<span class="error">Enter a valid name</span>');
      status = true;
    }
    if (surname.length < 1) {
      $('#surname').after('<span class="error">This field is required</span>');
      status = true;
    } else if (!nameRegex.test(surname)) {
      $('#surname').after('<span class="error">Enter a valid surname</span>');
      status = true;
    }
    if (email.length < 1) {
      $('#email').after('<span class="error">This field is required</span>');
      status = true;
    } else if (!regEx.test(email)) {
      $('#email').after('<span class="error">Enter a valid email</span>');
      status = true;
    }
    if (password.length < 8) {
      $('#password').after('<span class="error">Password must be at least 8 characters long</span>');
      status = true;
    } else if (!passwordRegex.test(password)) {
      $('#password').after('<span class="error">Password not valid</span>');
    }
    if (repeatPassword.length < 8) {
      $('#repeat_password').after('<span class="error">Repeat password must be at least 8 characters long</span>');
      status = true;
    } else if (password != repeatPassword) {
      $('#repeat_password').after('<span class="error">Repeat password must be equals to password</span>');
      status = true;
    }

    return status ? false : true;
  });

});