$(document).ready(function() {

    $('#loginForm').submit(function() {
        var login = $('#login').val();
        var password = $('#password').val();
        var nameRegex = /^\p{L}{3,20}$/u;
        var passwordRegex = /^\w{8,26}$/;
        var status = false;
        
        $('.error').remove();

        if (login.length < 1) {
            $('#login').after('<span class="error">This field is required</span>');
            status = true;
        } else if (!nameRegex.test(login)) {
            $('#login').after('<span class="error">Enter a valid login</span>');
            status = true;
        }
        if (password.length < 8) {
            $('#password').after('<span class="error">Password must be at least 8 characters long</span>');
            status = true;
        } else if (!passwordRegex.test(password)) {
            $('#password').after('<span class="error">Password not valid</span>');
        }

        return status ? false : true;
    });
    
});