function check() {
      var email = document.getElementById("email").value;
      var password = document.getElementById("password").value;
      var repeatPassword = document.getElementById("repeat_password").value;
      var passwordRegex = /^\w{8,26}$/;
      var regEx = /^\w{0,63}@(?:\w{1,63}\.){1,125}[a-z]{2,63}$/;
      var emailDiv = document.getElementById("emailDiv");
      var passwordDiv = document.getElementById("passwordDiv");
      var repeatPasswordDiv = document.getElementById("repeatPasswordDiv");
      var status = false;

      deletePreviousErrors();

      if (email.length < 1) {
            createError("Email field is required", emailDiv);
            status = true;
      } else if (!regEx.test(email)) {
            createError("Enter a valid email", emailDiv);
            status = true;
      }

      if (password.length < 8) {
            createError("Password must be at least 8 characters long", passwordDiv);
            status = true;
      } else if (!passwordRegex.test(password)) {
            createError("Password not valid", passwordDiv);
            status = true;
      }

      if (repeatPassword.length < 8) {
            createError("Repeat password must be at least 8 characters long", repeatPasswordDiv);
            status = true;
      } else if (password != repeatPassword) {
            createError("Repeat password must be equals to password", repeatPasswordDiv);
            status = true;
      }

      return status ? false : true;
}

function deletePreviousErrors() {
      var errors = document.getElementsByClassName("error");

      while (errors.length != 0) {
            errors[0].parentNode.removeChild(errors[0]);

      }
}

function createError(message, div) {
      var dateSpan = document.createElement('span');
      dateSpan.className = "error";
      dateSpan.innerHTML = message;
      div.appendChild(dateSpan);
}
