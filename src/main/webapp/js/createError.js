function serverValidate(errors) {
    if (errors != "") {
        var errorsMap = splitString(errors);
        errorsMap.forEach(function (value, key) {
            $("#" + key).after('<span class="error">' + value + '</span>');
        });
    }
}

function splitString(splitElement) {
    var map = new Map();
    splitElement.substring(1, splitElement.length - 1)
        .split(", ").forEach(element => {
            let entry = element.split("=");
            map.set(entry[0], entry[1]);
        });
    return map;
}