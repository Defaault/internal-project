package com.epam.ipatov.artem.listener;

import com.epam.ipatov.artem.container.ProviderContainer;
import com.epam.ipatov.artem.container.StorageContainer;
import com.epam.ipatov.artem.dao.impl.ProductDaoImpl;
import com.epam.ipatov.artem.dao.impl.UserDaoImpl;
import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.provider.impl.CookieKeyProvider;
import com.epam.ipatov.artem.provider.impl.HiddenFieldKeyProvider;
import com.epam.ipatov.artem.repository.UserRepository;
import com.epam.ipatov.artem.repository.impl.UserRepositoryImpl;
import com.epam.ipatov.artem.service.impl.FileUploadServiceImpl;
import com.epam.ipatov.artem.service.impl.ProductServiceImpl;
import com.epam.ipatov.artem.service.impl.UserServiceTransactionImpl;
import com.epam.ipatov.artem.storage.impl.ContextCaptchaStorage;
import com.epam.ipatov.artem.storage.impl.SessionCaptchaStorage;
import com.epam.ipatov.artem.transaction.TransactionManager;
import lombok.extern.log4j.Log4j;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_STORAGE;
import static com.epam.ipatov.artem.constant.Constant.CONTEXT_STORAGE;
import static com.epam.ipatov.artem.constant.Constant.COOKIE_PROVIDER;
import static com.epam.ipatov.artem.constant.Constant.FILE_UPLOAD_SERVICE;
import static com.epam.ipatov.artem.constant.Constant.HIDDEN_FIELD_PROVIDER;
import static com.epam.ipatov.artem.constant.Constant.KEY_PROVIDER;
import static com.epam.ipatov.artem.constant.Constant.PRODUCT_SERVICE;
import static com.epam.ipatov.artem.constant.Constant.PROVIDER;
import static com.epam.ipatov.artem.constant.Constant.SESSION_STORAGE;
import static com.epam.ipatov.artem.constant.Constant.STORAGE;
import static com.epam.ipatov.artem.constant.Constant.USER_SERVICE;

/**
 * AppContextListener
 */
@Log4j
@WebListener
public class AppContextListener implements ServletContextListener {

    private StorageContainer storageContainer;
    private ProviderContainer providerContainer;

    /**
     * Init users, storage and provider for captcha, and inject service and repository dependencies.
     *
     * @param sce - servletContextEvent.
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("Context initialization");
        ServletContext context = sce.getServletContext();
        DataSource dataSource = initDataSource();
        TransactionManager transactionManager = new TransactionManager(dataSource);
        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        UserDaoImpl userDao = new UserDaoImpl();
        ProductDaoImpl productDao = new ProductDaoImpl(dataSource);

        initProviderContainer();
        initStorageContainer(context, providerContainer);
        initUsers(userRepository);
        initCaptchaImpl(context);

        context.setAttribute(PRODUCT_SERVICE, new ProductServiceImpl(productDao));
        context.setAttribute(USER_SERVICE, new UserServiceTransactionImpl(userDao, transactionManager));
        context.setAttribute(FILE_UPLOAD_SERVICE, new FileUploadServiceImpl());
        log.info("Context has been initialized");
    }

    /**
     * Init storage and provider for captcha.
     *
     * @param context - servlet context.
     */
    private void initCaptchaImpl(ServletContext context) {
        log.info("Initialization captcha implementation");
        String typeProvider = context.getInitParameter(PROVIDER);
        String typeStorage = context.getInitParameter(STORAGE);

        context.setAttribute(KEY_PROVIDER, providerContainer.get(typeProvider));
        context.setAttribute(CAPTCHA_STORAGE, storageContainer.get(typeStorage));
    }

    /**
     * Init 3 users.
     *
     * @param userRepository - userRepository.
     */
    private void initUsers(UserRepository userRepository) {
        User firstUser = User.builder()
                .login("default")
                .email("qwe@qwe.qwe")
                .name("Artem")
                .surname("Ipatov")
                .password("qweqweqwe")
                .build();
        User secondUser = User.builder()
                .login("sosed")
                .email("qwee@qwe.qwe")
                .name("Dima")
                .surname("Lopost")
                .password("qweqweqwe")
                .build();
        User thirdUser = User.builder()
                .login("nesosed")
                .email("ewq@qwe.qwe")
                .name("Vasya")
                .surname("Sosedov")
                .password("qweqweqwe")
                .build();

        userRepository.save(firstUser);
        userRepository.save(secondUser);
        userRepository.save(thirdUser);
        log.info("Users has been added to repository");
    }

    /**
     * Init provider container.
     */
    private void initProviderContainer() {
        providerContainer = new ProviderContainer();

        providerContainer.set(COOKIE_PROVIDER, new CookieKeyProvider());
        providerContainer.set(HIDDEN_FIELD_PROVIDER, new HiddenFieldKeyProvider());
    }

    /**
     * Init storage container.
     *
     * @param context           - context.
     * @param providerContainer - provider container.
     */
    private void initStorageContainer(ServletContext context, ProviderContainer providerContainer) {
        String typeProvider = context.getInitParameter(PROVIDER);
        storageContainer = new StorageContainer();

        storageContainer.set(SESSION_STORAGE, new SessionCaptchaStorage());
        storageContainer.set(CONTEXT_STORAGE, new ContextCaptchaStorage(providerContainer.get(typeProvider)));
    }

    /**
     * Init connection pool and dataSource.
     *
     * @return - dataSource.
     */
    private DataSource initDataSource() {
        DataSource dataSource = null;

        try {
            Context envCtx = (Context) (new InitialContext().lookup("java:comp/env"));
            dataSource = (DataSource) envCtx.lookup("jdbc/webApp");
        } catch (NamingException e) {
            log.error(e.getMessage());
        }

        return dataSource;
    }
}
