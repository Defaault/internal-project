package com.epam.ipatov.artem.constant;

import java.io.File;

/**
 * Constants
 */
public final class Constant {

    public static final int MIN_TIMEOUT = 100;
    public static final String ID = "id";
    public static final String ERRORS = "errors";
    public static final String FORM = "form";
    public static final String LOGIN = "login";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String REPEAT_PASSWORD = "repeat_password";
    public static final String SPAM = "spam";
    public static final String ON = "on";
    public static final String CAPTCHA = "captcha";
    public static final String CAPTCHA_INPUT = "captcha_input";
    public static final String USER_SERVICE = "userService";
    public static final String PRODUCT_SERVICE = "productService";
    public static final String FILE_UPLOAD_SERVICE = "fileUploadService";
    public static final String CAPTCHA_STORAGE = "captchaStorage";
    public static final String KEY_PROVIDER = "keyProvider";
    public static final String SESSION_STORAGE = "sessionStorage";
    public static final String CONTEXT_STORAGE = "contextStorage";
    public static final String COOKIE_PROVIDER = "cookieProvider";
    public static final String HIDDEN_FIELD_PROVIDER = "hiddenFieldProvider";
    public static final String STORAGE = "storage";
    public static final String PROVIDER = "provider";
    public static final String TIMEOUT = "timeout";
    public static final String CAPTCHA_IMAGE = "captchaImage";
    public static final String PICTURE = "picture";
    public static final String USER = "user";
    public static final String LOGIN_FORM = "loginForm";
    public static final String USER_DIR = "user.dir";
    public static final String AVATARS = "avatars";
    public static final String POINT = ".";
    public static final String SEPARATOR = "/";
    public static final String OCTET_STREAM = "octet-stream";
    public static final String MAIL = "mail";

    //Filter
    public static final String MANUFACTURER = "manufacturer";
    public static final String CATEGORY = "category";

    //URL
    public static final String LOGOUT_URL = "/logout";
    public static final String DRINKS_URL = "/drinks";
    public static final String AVATAR_URL = "/avatar";
    public static final String CAPTCHA_URL = "/captcha";
    public static final String LOGIN_URL = "/login";
    public static final String REGISTER_URL = "/register";
    public static final String INDEX_JSP = "index.jsp";
    public static final String PAGE_REGISTER_JSP = "/WEB-INF/jsp/page-register.jsp";
    public static final String PAGE_LOGIN_JSP = "/WEB-INF/jsp/page-login.jsp";
    public static final String PAGE_DRINKS_JSP = "/WEB-INF/jsp/page-products.jsp";

    //PATH
    public static final String AVATAR_PATH = System.getProperty(USER_DIR) + File.separator + AVATARS;

    private Constant() {
    }
}
