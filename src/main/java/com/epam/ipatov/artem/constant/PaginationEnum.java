package com.epam.ipatov.artem.constant;

import lombok.Getter;

/**
 * PaginationEnum
 */
@Getter
public enum PaginationEnum {
    FIVE(5), TEN(10);

    private int count;

    PaginationEnum(int count) {
        this.count = count;
    }
}
