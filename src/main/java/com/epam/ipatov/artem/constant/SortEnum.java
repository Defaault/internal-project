package com.epam.ipatov.artem.constant;

import lombok.Getter;

/**
 * SortEnum
 */
@Getter
public enum SortEnum {
    NAME_ASC("name asc"), NAME_DESC("name desc"), PRICE_ASC("price asc"), PRICE_DESC("price desc");

    private String name;

    SortEnum(String name) {
        this.name = name;
    }
}
