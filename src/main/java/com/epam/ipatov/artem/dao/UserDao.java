package com.epam.ipatov.artem.dao;

import com.epam.ipatov.artem.entity.User;

import java.sql.Connection;

/**
 * UserDao
 */
public interface UserDao {

    /**
     * Save user.
     *
     * @param user       - user.
     * @param connection - connection.
     * @return - void.
     */
    Void save(User user, Connection connection);

    /**
     * Get user by login.
     *
     * @param login      - login.
     * @param connection - connection.
     * @return - user.
     */
    User getByLogin(String login, Connection connection);

    /**
     * Delete user by login.
     *
     * @param login      - login.
     * @param connection - connection.
     * @return - void.
     */
    Void delete(String login, Connection connection);
}
