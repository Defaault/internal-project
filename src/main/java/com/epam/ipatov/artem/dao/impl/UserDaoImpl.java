package com.epam.ipatov.artem.dao.impl;

import com.epam.ipatov.artem.dao.UserDao;
import com.epam.ipatov.artem.entity.User;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.MAIL;
import static com.epam.ipatov.artem.constant.Constant.NAME;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.PICTURE;
import static com.epam.ipatov.artem.constant.Constant.SURNAME;

@Log4j
public class UserDaoImpl implements UserDao {

    private static final String SAVE_USER = "insert into users(id, login, password, name, surname, mail, picture) value (default,?,?,?,?,?,?)";
    private static final String GET_USER_BY_LOGIN = "select * from users where login = ?";
    private static final String DELETE_USER_BY_LOGIN = "delete from users where login = ?";

    @Override
    public Void save(User user, Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_USER)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getName());
            preparedStatement.setString(4, user.getSurname());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setString(6, user.getPicture());
            preparedStatement.execute();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public User getByLogin(String login, Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return User.builder()
                            .login(resultSet.getString(LOGIN))
                            .password(resultSet.getString(PASSWORD))
                            .name(resultSet.getString(NAME))
                            .surname(resultSet.getString(SURNAME))
                            .email(resultSet.getString(MAIL))
                            .picture(resultSet.getString(PICTURE))
                            .build();
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public Void delete(String login, Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            preparedStatement.execute();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
