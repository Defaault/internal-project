package com.epam.ipatov.artem.dao.impl;

import com.epam.ipatov.artem.builder.ProductQueryBuilder;
import com.epam.ipatov.artem.dao.ProductDao;
import com.epam.ipatov.artem.entity.Product;
import com.epam.ipatov.artem.entity.dto.FilterData;
import lombok.extern.log4j.Log4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.ipatov.artem.constant.Constant.CATEGORY;
import static com.epam.ipatov.artem.constant.Constant.ID;
import static com.epam.ipatov.artem.constant.Constant.MANUFACTURER;
import static com.epam.ipatov.artem.constant.Constant.NAME;

@Log4j
public class ProductDaoImpl implements ProductDao {

    private static final String GET_ALL_MANUFACTURED = "select distinct manufacturer from products";
    private static final String GET_ALL_CATEGORIES = "select distinct category from products";
    private static final String PRICE = "price";

    private DataSource dataSource;

    public ProductDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Product> findAllByPredicate(FilterData data, int firstProduct, int lastProduct) {
        List<Product> products = new ArrayList<>();
        String query = predicateQuery(data);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query + String.format(" limit %s, %s", firstProduct, lastProduct));
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                Product product = Product.builder()
                        .id(resultSet.getInt(ID))
                        .name(resultSet.getString(NAME))
                        .category(resultSet.getString(CATEGORY))
                        .manufacturer(resultSet.getString(MANUFACTURER))
                        .price(resultSet.getInt(PRICE))
                        .build();

                products.add(product);
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return products;
    }

    @Override
    public List<String> findAllManufactured() {
        List<String> manufactures = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_MANUFACTURED);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                manufactures.add(resultSet.getString(MANUFACTURER));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return manufactures;
    }

    @Override
    public List<String> findAllCategory() {
        List<String> categories = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_CATEGORIES);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                categories.add(resultSet.getString(CATEGORY));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return categories;
    }

    @Override
    public int getCountByPredicate(FilterData filterData) {
        String query = predicateQuery(filterData);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            if (resultSet.last()) {
                return resultSet.getRow();
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    /**
     * Create query by filter data.
     *
     * @param data - filter data.
     * @return - query.
     */
    private String predicateQuery(FilterData data) {
        return ProductQueryBuilder.builder()
                .name(data.getName())
                .category(data.getCategory())
                .manufacturer(data.getManufacturer())
                .order(data.getOrderBy())
                .price(data.getMinPrice(), data.getMaxPrice())
                .build();
    }
}
