package com.epam.ipatov.artem.dao;

import com.epam.ipatov.artem.entity.Product;
import com.epam.ipatov.artem.entity.dto.FilterData;

import java.util.List;

/**
 * ProductDao
 */
public interface ProductDao {

    /**
     * Find all products by predicate.
     *
     * @param statement    - predicate statement.
     * @param firstProduct - index of first product.
     * @param lastProduct  - index of last product.
     * @return - list of products.
     */
    List<Product> findAllByPredicate(FilterData data, int firstProduct, int lastProduct);

    /**
     * Find all manufactures.
     *
     * @return - list of string.
     */
    List<String> findAllManufactured();

    /**
     * Find all categories.
     *
     * @return - list of string.
     */
    List<String> findAllCategory();

    int getCountByPredicate(FilterData filterData);
}
