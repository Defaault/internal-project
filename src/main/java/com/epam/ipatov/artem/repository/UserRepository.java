package com.epam.ipatov.artem.repository;

import com.epam.ipatov.artem.entity.User;

/**
 * UserRepository
 */
public interface UserRepository {

    /**
     * Save user.
     *
     * @param user - user.
     */
    void save(User user);

    /**
     * Find user by login.
     *
     * @param login - login.
     * @return - user.
     */
    User find(String login);

    /**
     * Delete user by login.
     *
     * @param login - login.
     */
    void delete(String login);
}
