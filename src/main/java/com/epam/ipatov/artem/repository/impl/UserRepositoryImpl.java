package com.epam.ipatov.artem.repository.impl;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * UserRepositoryImpl
 */
public class UserRepositoryImpl implements UserRepository {

    private List<User> users;

    public UserRepositoryImpl() {
        users = new ArrayList<>();
    }

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public User find(String login) {
        return users.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void delete(String login) {
        users.removeIf(u -> login.equals(u.getLogin()));
    }
}
