package com.epam.ipatov.artem.converter;

import com.epam.ipatov.artem.constant.PaginationEnum;
import com.epam.ipatov.artem.entity.dto.FilterData;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import static com.epam.ipatov.artem.constant.Constant.CATEGORY;
import static com.epam.ipatov.artem.constant.Constant.MANUFACTURER;
import static com.epam.ipatov.artem.constant.Constant.NAME;
import static java.util.Objects.nonNull;
import static java.util.regex.Pattern.matches;

/**
 * FilterDataConverter
 */
public class FilterDataConverter {

    private static final String PAGE = "page";
    private static final String MIN_PRICE = "minPrice";
    private static final String MAX_PRICE = "maxPrice";
    private static final String COUNT = "count";
    private static final String ORDER_BY = "orderBy";
    private static final String DIGIT_PATTERN = "\\p{Digit}+";

    public FilterDataConverter() {
    }

    /**
     * Create and fill filter data.
     *
     * @param request - request.
     * @return - filter data.
     */
    public FilterData createData(HttpServletRequest request) {
        String name = request.getParameter(NAME);
        String[] manufacturer = request.getParameterValues(MANUFACTURER);
        String[] category = request.getParameterValues(CATEGORY);
        String minPrice = request.getParameter(MIN_PRICE);
        String maxPrice = request.getParameter(MAX_PRICE);
        String count = request.getParameter(COUNT);
        String orderBy = request.getParameter(ORDER_BY);
        String page = request.getParameter(PAGE);

        return FilterData.builder()
                .name(name)
                .manufacturer(getSelectList(manufacturer))
                .category(getSelectList(category))
                .minPrice(getMinPrice(minPrice))
                .maxPrice(getMaxPrice(minPrice, maxPrice))
                .count(getCount(count))
                .orderBy(orderBy)
                .page(getPage(page))
                .build();
    }

    /**
     * Convert string to int if it contains only numbers, or return 0.
     *
     * @param param - param.
     * @return - int presentation of string param.
     */
    private int getMinPrice(String param) {
        return nonNull(param) && matches(DIGIT_PATTERN, param) ? Integer.parseInt(param) : 0;
    }

    /**
     * Convert string maxParam to int if it contains only values, or return Integer max value,
     * or if it lower than min value - set value of min param.
     *
     * @param minParam - minParam.
     * @param maxParam - maxParam.
     * @return - int presentation of string maxParam.
     */
    private int getMaxPrice(String minParam, String maxParam) {
        int minValue = 0;
        int maxValue = Integer.MAX_VALUE;

        if (nonNull(minParam) && matches(DIGIT_PATTERN, minParam)) {
            minValue = Integer.parseInt(minParam);
        }
        if (nonNull(minParam) && matches(DIGIT_PATTERN, maxParam)) {
            maxValue = Integer.parseInt(maxParam);
        }
        if (minValue > maxValue) {
            maxValue = minValue;
        }
        return maxValue;
    }

    /**
     * Convert string presentation of count page regarding PaginationEnum, if it doesn't exist -
     * set up default value 5.
     *
     * @param count - count.
     * @return - int presentation of string count.
     */
    private int getCount(String count) {
        return EnumSet.allOf(PaginationEnum.class).stream()
                .map(PaginationEnum::getCount)
                .filter(integer -> String.valueOf(integer).equals(count))
                .findFirst()
                .orElse(PaginationEnum.FIVE.getCount());
    }

    /**
     * Convert string page to int value if it contains only numbers.
     *
     * @param page - page.
     * @return - int presentation of page.
     */
    private int getPage(String page) {
        return nonNull(page) && matches(DIGIT_PATTERN, page) ? Integer.parseInt(page) : 1;
    }

    /**
     * Create list of params array, if it == null - return null.
     *
     * @param params - params.
     * @return - list of array.
     */
    private List<String> getSelectList(String[] params) {
        return nonNull(params) ? Arrays.asList(params) : null;
    }
}
