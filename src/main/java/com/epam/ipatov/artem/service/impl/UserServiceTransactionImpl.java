package com.epam.ipatov.artem.service.impl;


import com.epam.ipatov.artem.dao.UserDao;
import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.service.UserService;
import com.epam.ipatov.artem.transaction.TransactionManager;

/**
 * UserServiceTransactionImpl
 */
public class UserServiceTransactionImpl implements UserService {

    private UserDao userDao;
    private TransactionManager transactionManager;

    public UserServiceTransactionImpl(UserDao userDao, TransactionManager transactionManager) {
        this.userDao = userDao;
        this.transactionManager = transactionManager;
    }

    @Override
    public void create(User user) {
        transactionManager.execute(connection -> userDao.save(user, connection));
    }

    @Override
    public User getUser(String login) {
        return transactionManager.execute(connection -> userDao.getByLogin(login, connection));
    }

    @Override
    public void remove(String login) {
        transactionManager.execute(connection -> userDao.delete(login, connection));
    }
}
