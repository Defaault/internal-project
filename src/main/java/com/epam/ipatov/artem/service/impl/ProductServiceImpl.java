package com.epam.ipatov.artem.service.impl;

import com.epam.ipatov.artem.dao.ProductDao;
import com.epam.ipatov.artem.entity.Product;
import com.epam.ipatov.artem.entity.dto.FilterData;
import com.epam.ipatov.artem.service.ProductService;
import lombok.extern.log4j.Log4j;

import java.util.List;

@Log4j
public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;

    public ProductServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<String> getAllManufactures() {
        return productDao.findAllManufactured();
    }

    @Override
    public List<String> getAllCategory() {
        return productDao.findAllCategory();
    }

    @Override
    public int getCountByPredicate(FilterData filterData) {
        return (int) Math.ceil(productDao.getCountByPredicate(filterData) * 1.0 / filterData.getCount());
    }

    @Override
    public List<Product> getAllByPredicate(FilterData data) {
        int startProduct = data.getCount() * data.getPage() - data.getCount();
        int endProduct = startProduct + data.getCount();

        return productDao.findAllByPredicate(data, startProduct, endProduct);
    }
}
