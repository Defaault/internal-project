package com.epam.ipatov.artem.service.impl;

import com.epam.ipatov.artem.service.FileUploadService;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

import static com.epam.ipatov.artem.constant.Constant.AVATARS;
import static com.epam.ipatov.artem.constant.Constant.OCTET_STREAM;
import static com.epam.ipatov.artem.constant.Constant.POINT;
import static com.epam.ipatov.artem.constant.Constant.SEPARATOR;
import static com.epam.ipatov.artem.constant.Constant.USER_DIR;
import static java.util.Objects.nonNull;

/**
 * FileUploadServiceImpl
 */
@Log4j
public class FileUploadServiceImpl implements FileUploadService {

    @Override
    public String upload(Part part, String login, String to) {
        log.info("Start upload file");
        String fileName = null;
        if (nonNull(part)) {
            String avatarPackage = System.getProperty(USER_DIR) + File.separator + AVATARS + File.separator;
            String contentType = part.getContentType();
            String extension = contentType.substring(contentType.lastIndexOf(SEPARATOR) + 1);
            File uploadDir = new File(to);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            if (!OCTET_STREAM.equals(extension)) {
                try {
                    part.write(avatarPackage + login + POINT + extension);
                    fileName = login + POINT + extension;
                    log.info("Avatar has been loaded");
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
        }
        log.info("File has been uploaded");
        return fileName;
    }
}
