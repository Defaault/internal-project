package com.epam.ipatov.artem.service;

import javax.servlet.http.Part;

/**
 * FileUploadService
 */
public interface FileUploadService {

    /**
     * Upload file from form.
     *
     * @param part  - part.
     * @param login - login.
     * @param to    - location of files.
     * @return -file name.
     */
    String upload(Part part, String login, String to);
}
