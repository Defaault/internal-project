package com.epam.ipatov.artem.service;

import com.epam.ipatov.artem.entity.User;

/**
 * UserService
 */
public interface UserService {

    /**
     * Create user.
     *
     * @param user - user.
     */
    void create(User user);

    /**
     * Get user by login.
     *
     * @param login - login.
     * @return - user.
     */
    User getUser(String login);

    /**
     * Remove user by login.
     *
     * @param login - login.
     */
    void remove(String login);
}
