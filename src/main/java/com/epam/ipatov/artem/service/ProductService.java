package com.epam.ipatov.artem.service;

import com.epam.ipatov.artem.entity.Product;
import com.epam.ipatov.artem.entity.dto.FilterData;

import java.util.List;

/**
 * ProductService
 */
public interface ProductService {

    /**
     * Get all manufactures.
     *
     * @return - list of string.
     */
    List<String> getAllManufactures();

    /**
     * Get all categories.
     *
     * @return - list of string.
     */
    List<String> getAllCategory();

    /**
     * Get count of all notes by predicate.
     *
     * @param filterData - filter data dto.
     * @return - count.
     */
    int getCountByPredicate(FilterData filterData);

    /**
     * Get all products by predicate.
     *
     * @param data - filter data dto.
     * @return - list of products.
     */
    List<Product> getAllByPredicate(FilterData data);
}
