package com.epam.ipatov.artem.service.impl;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.repository.UserRepository;
import com.epam.ipatov.artem.service.UserService;

/**
 * UserServiceImpl
 */
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void create(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUser(String login) {
        return userRepository.find(login);
    }

    @Override
    public void remove(String login) {
        userRepository.delete(login);
    }
}
