package com.epam.ipatov.artem.provider.impl;

import com.epam.ipatov.artem.provider.KeyProvider;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.epam.ipatov.artem.constant.Constant.ID;

/**
 * HiddenFieldKeyProvider
 */
@Log4j
public class HiddenFieldKeyProvider implements KeyProvider {

    @Override
    public void set(HttpServletRequest request, HttpServletResponse response, String id) {
        log.info("Setting id in session");
        HttpSession session = request.getSession();
        session.setAttribute(ID, id);
    }

    @Override
    public String get(HttpServletRequest request) {
        log.info("Getting id from hidden field");
        return request.getParameter(ID);
    }
}
