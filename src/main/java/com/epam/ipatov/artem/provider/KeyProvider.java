package com.epam.ipatov.artem.provider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * KeyProvider
 */
public interface KeyProvider {

    /**
     * Set captcha id relatively chose provider.
     *
     * @param request  - request.
     * @param response - response.
     * @param id       - id.
     */
    void set(HttpServletRequest request, HttpServletResponse response, String id);

    /**
     * Get captcha id regarding chose provider.
     *
     * @param request - request.
     * @return - id.
     */
    String get(HttpServletRequest request);
}
