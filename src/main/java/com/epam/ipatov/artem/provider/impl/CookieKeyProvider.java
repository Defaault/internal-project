package com.epam.ipatov.artem.provider.impl;

import com.epam.ipatov.artem.provider.KeyProvider;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import static com.epam.ipatov.artem.constant.Constant.ID;
import static java.util.Objects.nonNull;

/**
 * CookieKeyProvider
 */
@Log4j
public class CookieKeyProvider implements KeyProvider {

    @Override
    public void set(HttpServletRequest request, HttpServletResponse response, String id) {
        log.info("Setting cookie");
        Cookie cookie = new Cookie(ID, id);
        response.addCookie(cookie);
    }

    @Override
    public String get(HttpServletRequest request) {
        log.info("Getting id from cookie");
        Cookie cookie = Arrays.stream(request.getCookies())
                .filter(c -> ID.equals(c.getName()))
                .findFirst().orElse(null);

        return nonNull(cookie) ? cookie.getValue() : null;
    }
}
