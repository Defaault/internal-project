package com.epam.ipatov.artem.validate;

import com.epam.ipatov.artem.entity.dto.CheckinUser;
import com.epam.ipatov.artem.entity.dto.LoginUser;
import ml.miron.captcha.image.Captcha;

import java.util.HashMap;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_INPUT;
import static com.epam.ipatov.artem.constant.Constant.EMAIL;
import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.NAME;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.REPEAT_PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.SURNAME;
import static java.util.Objects.isNull;
import static java.util.regex.Pattern.matches;

/**
 * UserValidate
 */
public final class UserValidate {

    private UserValidate() {
    }

    /**
     * Validate form-data
     *
     * @param user    - user dto.
     * @param captcha - captcha.
     * @return - map of errors.
     */
    public static Map<String, String> validate(CheckinUser user, Captcha captcha) {
        Map<String, String> errors = new HashMap<>();

        if (isNull(user.getLogin()) || !matches("\\w{3,20}", user.getLogin())) {
            errors.put(LOGIN, "The length of the login must be from 3 to 20 Latin characters or numbers.");
        }
        if (isNull(user.getEmail()) || !matches("\\w{0,63}@(?:\\w{1,63}\\.){1,125}[a-z]{2,63}", user.getEmail())) {
            errors.put(EMAIL, "Email address must contain @ and .");
        }
        if (isNull(user.getPassword()) || !matches("\\w{8,26}", user.getPassword())) {
            errors.put(PASSWORD, "The length of the password must be from 8 to 26 Latin characters or numbers.");
        }
        if (isNull(user.getRepeatPassword()) || !matches("\\w{8,26}", user.getRepeatPassword())) {
            errors.put(REPEAT_PASSWORD, "The length of the confirm password must be from 8 to 26 Latin characters or numbers.");
        } else if (!user.getRepeatPassword().equals(user.getPassword())) {
            errors.put(REPEAT_PASSWORD, "Passwords do not match.");
        }
        if (isNull(user.getName()) || !matches("\\p{L}{3,20}", user.getName())) {
            errors.put(NAME, "The length of the name must be from 3 to 20 Latin characters.");
        }
        if (isNull(user.getSurname()) || !matches("\\p{L}{3,20}", user.getSurname())) {
            errors.put(SURNAME, "The length of the surname must be from 3 to 20 Latin characters.");
        }
        if (isNull(user.getCaptcha()) || isNull(captcha) || !captcha.isCorrect(user.getCaptcha())) {
            errors.put(CAPTCHA_INPUT, "Captcha incorrect.");
        }

        return errors;
    }

    /**
     * Validate login data.
     *
     * @param user - user.
     * @return - map of errors.
     */
    public static Map<String, String> validate(LoginUser user) {
        Map<String, String> errors = new HashMap<>();

        if (isNull(user.getLogin()) || !matches("\\w{3,20}", user.getLogin())) {
            errors.put(LOGIN, "The length of the login must be from 3 to 20 Latin characters or numbers.");
        }
        if (isNull(user.getPassword()) || !matches("\\w{8,26}", user.getPassword())) {
            errors.put(PASSWORD, "The length of the password must be from 8 to 26 Latin characters or numbers.");
        }

        return errors;
    }
}
