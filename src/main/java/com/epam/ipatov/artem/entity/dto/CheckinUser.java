package com.epam.ipatov.artem.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CheckinUser {

    private String login;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String repeatPassword;
    private Boolean spam;
    private String captcha;

}
