package com.epam.ipatov.artem.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FilterData {

    private String name;
    private List<String> manufacturer;
    private List<String> category;
    private int minPrice;
    private int maxPrice;
    private int count;
    private String orderBy;
    private int page;
}
