package com.epam.ipatov.artem.entity;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private String login;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String picture;
}
