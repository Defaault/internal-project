package com.epam.ipatov.artem.util;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.entity.dto.CheckinUser;

import static com.epam.ipatov.artem.util.PasswordEncoderUtil.encodePassword;

/**
 * ConvertUtil
 */
public final class ConvertUtil {

    private ConvertUtil() {
    }

    /**
     * Convert checkinUser dto in user entity.
     *
     * @param checkinUser - checkin user dto.
     * @param fileName    - fileName.
     * @return - user.
     */
    public static User convert(CheckinUser checkinUser, String fileName) {
        return User.builder()
                .name(checkinUser.getName())
                .surname(checkinUser.getSurname())
                .login(checkinUser.getLogin())
                .email(checkinUser.getEmail())
                .picture(fileName)
                .password(encodePassword(checkinUser.getPassword()))
                .build();
    }
}
