package com.epam.ipatov.artem.util;

import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * Password encoder. Works with MessageDigest SHA-256.
 */
public final class PasswordEncoderUtil {

    private static final Logger log = Logger.getLogger(PasswordEncoderUtil.class);

    private PasswordEncoderUtil() {
    }

    /**
     * Encode password.
     *
     * @param password user password.
     * @return hash string.
     */
    public static String encodePassword(String password) {
        try {
            return hash(password);
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * Matches passwords.
     *
     * @param password unencrypted password.
     * @param match    encrypted password.
     * @return status of match.
     */
    public static boolean matches(String password, String match) {
        try {
            if (Objects.equals(hash(password), match)) {
                return true;
            }
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * Password hashing.
     *
     * @param password user password.
     * @return password hash.
     * @throws NoSuchAlgorithmException if the given algorithm does not exist.
     */
    private static String hash(String password) throws NoSuchAlgorithmException {
        StringBuilder line = new StringBuilder();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(password.getBytes());

        for (byte b : hash) {
            String s = Integer.toHexString(0xff & b);
            s = (s.length() == 1) ? ("0" + s) : s;
            line.append(s);
        }
        return line.toString();
    }
}
