package com.epam.ipatov.artem.builder;

import lombok.extern.log4j.Log4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * ProductQueryBuilder
 */
@Log4j
public class ProductQueryBuilder {

    private static final String COMA = ", ";
    private static final String WHERE = " where ";
    private static final String AND = " and ";
    private static final String SELECT_ALL_PRODUCTS = "select * from products ";

    private StringBuilder query;
    private List<String> predicates;
    private String orderBy;

    private ProductQueryBuilder() {
        query = new StringBuilder(SELECT_ALL_PRODUCTS);
        predicates = new ArrayList<>();
    }

    /**
     * Return new ProductQueryBuilder
     *
     * @return - ProductQueryBuilder.
     */
    public static ProductQueryBuilder builder() {
        return new ProductQueryBuilder();
    }

    /**
     * Set up price predicate.Default min = 0, max = Integer.MAX_VALUE
     *
     * @param minPrice - min price.
     * @param maxPrice - max price.
     * @return - ProductQueryBuilder.
     */
    public ProductQueryBuilder price(int minPrice, int maxPrice) {
        predicates.add(String.format("price > %s", minPrice));
        predicates.add(String.format("price < %s ", maxPrice));

        return this;
    }

    /**
     * Set up name predicate.
     *
     * @param name - name.
     * @return - ProductQueryBuilder.
     */
    public ProductQueryBuilder name(String name) {
        if (isNotBlank(name)) {
            predicates.add(String.format("name like '%%%s%%'", name));
        }

        return this;
    }

    /**
     * Set up categories predicate.
     *
     * @param category - category.
     * @return - ProductQueryBuilder.
     */
    public ProductQueryBuilder category(List<String> category) {
        if (nonNull(category)) {
            String categories = category.stream()
                    .map(s -> String.format("'%s'", s))
                    .collect(Collectors.joining(COMA));

            predicates.add(String.format("category in (%s)", categories));
        }

        return this;
    }

    /**
     * Set up manufactures predicate.
     *
     * @param manufacturer - manufacturer.
     * @return - ProductQueryBuilder.
     */
    public ProductQueryBuilder manufacturer(List<String> manufacturer) {
        if (nonNull(manufacturer)) {
            String manufactures = manufacturer.stream()
                    .map(s -> String.format("'%s'", s))
                    .collect(Collectors.joining(COMA));

            predicates.add(String.format("manufacturer in (%s)", manufactures));
        }

        return this;
    }

    /**
     * Set up order predicate.
     *
     * @param order - order.
     * @return - ProductQueryBuilder.
     */
    public ProductQueryBuilder order(String order) {
        if (isNotBlank(order)) {
            orderBy = String.format("order by %s", order);
        }

        return this;
    }

    /**
     * Build predicate statement.
     *
     * @return - ProductQueryBuilder.
     */
    public String build() {
        if (predicates.size() > 0) {
            query.append(WHERE);
            String condition = String.join(AND, predicates);
            query.append(condition);
            if (isNotBlank(orderBy)) {
                query.append(orderBy);
            }
        }

        return query.toString();
    }
}
