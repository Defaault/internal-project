package com.epam.ipatov.artem.transaction;

import lombok.extern.log4j.Log4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Function;

import static java.util.Objects.nonNull;

/**
 * TransactionManager
 */
@Log4j
public class TransactionManager {

    private DataSource dataSource;

    public TransactionManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Wrap connection by transaction.
     *
     * @param function - dao method.
     * @param <R>      - type of result.
     * @return - result.
     */
    public <R> R execute(Function<Connection, R> function) {
        Connection connection = null;
        R result = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            result = function.apply(connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e);
            rollback(connection);
        } finally {
            close(connection);
        }
        return result;
    }

    /**
     * Close connection.
     *
     * @param connection - connection.
     */
    private void close(Connection connection) {
        try {
            if (nonNull(connection)) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Rollback connection.
     *
     * @param connection - connection.
     */
    private void rollback(Connection connection) {
        try {
            if (nonNull(connection)) {
                connection.rollback();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
