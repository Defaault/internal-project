package com.epam.ipatov.artem.container;

import com.epam.ipatov.artem.provider.KeyProvider;

import java.util.HashMap;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.COOKIE_PROVIDER;
import static java.util.Objects.isNull;

/**
 * ProviderContainer
 */
public class ProviderContainer {

    private Map<String, KeyProvider> map;

    public ProviderContainer() {
        map = new HashMap<>();
    }

    /**
     * Set provider in container.
     *
     * @param key      - key.
     * @param provider - provider.
     */
    public void set(String key, KeyProvider provider) {
        map.put(key, provider);
    }

    /**
     * Get provider from map.
     *
     * @param name - name of provider, if name - doesn't exits, return cookieProvider.
     * @return - provider.
     */
    public KeyProvider get(String name) {
        return isNull(map.get(name)) ? map.get(COOKIE_PROVIDER) : map.get(name);
    }
}
