package com.epam.ipatov.artem.container;

import com.epam.ipatov.artem.storage.CaptchaStorage;

import java.util.HashMap;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.SESSION_STORAGE;
import static java.util.Objects.isNull;

/**
 * StorageContainer
 */
public class StorageContainer {

    private Map<String, CaptchaStorage> map;

    public StorageContainer() {
        map = new HashMap<>();
    }

    /**
     * Set storage in container.
     *
     * @param key     - key.
     * @param storage - storage.
     */
    public void set(String key, CaptchaStorage storage) {
        map.put(key, storage);
    }

    /**
     * Get storage from map.
     *
     * @param name - name of storage, if name - doesn't exits, return sessionStorage.
     * @return - storage.
     */
    public CaptchaStorage get(String name) {
        return isNull(map.get(name)) ? map.get(SESSION_STORAGE) : map.get(name);
    }
}
