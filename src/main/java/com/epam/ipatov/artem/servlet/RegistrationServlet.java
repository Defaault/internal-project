package com.epam.ipatov.artem.servlet;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.entity.dto.CheckinUser;
import com.epam.ipatov.artem.service.FileUploadService;
import com.epam.ipatov.artem.service.UserService;
import com.epam.ipatov.artem.storage.CaptchaStorage;
import com.epam.ipatov.artem.util.ConvertUtil;
import com.epam.ipatov.artem.validate.UserValidate;
import lombok.extern.log4j.Log4j;
import ml.miron.captcha.image.Captcha;
import ml.miron.captcha.image.background.GradiatedBackground;
import ml.miron.captcha.image.producer.DefaultTextProducer;
import ml.miron.captcha.image.renderer.FishEyeRenderer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.AVATAR_PATH;
import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_IMAGE;
import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_INPUT;
import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_STORAGE;
import static com.epam.ipatov.artem.constant.Constant.EMAIL;
import static com.epam.ipatov.artem.constant.Constant.ERRORS;
import static com.epam.ipatov.artem.constant.Constant.FILE_UPLOAD_SERVICE;
import static com.epam.ipatov.artem.constant.Constant.FORM;
import static com.epam.ipatov.artem.constant.Constant.INDEX_JSP;
import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.NAME;
import static com.epam.ipatov.artem.constant.Constant.ON;
import static com.epam.ipatov.artem.constant.Constant.PAGE_REGISTER_JSP;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.PICTURE;
import static com.epam.ipatov.artem.constant.Constant.REGISTER_URL;
import static com.epam.ipatov.artem.constant.Constant.REPEAT_PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.SPAM;
import static com.epam.ipatov.artem.constant.Constant.SURNAME;
import static com.epam.ipatov.artem.constant.Constant.USER;
import static com.epam.ipatov.artem.constant.Constant.USER_SERVICE;
import static java.util.Objects.nonNull;


/**
 * RegistrationServlet
 */
@WebServlet(REGISTER_URL)
@Log4j
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5)
public class RegistrationServlet extends HttpServlet {

    private UserService userService;
    private FileUploadService fileUploadService;
    private CaptchaStorage captchaStorage;

    /**
     * Inject all service dependencies.
     *
     * @param config - servletConfig.
     * @throws ServletException - ServletException.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        log.info("Initialization of fields");
        ServletContext context = config.getServletContext();
        userService = (UserService) context.getAttribute(USER_SERVICE);
        fileUploadService = (FileUploadService) context.getAttribute(FILE_UPLOAD_SERVICE);
        captchaStorage = (CaptchaStorage) context.getAttribute(CAPTCHA_STORAGE);
        log.info("Fields has been initialized");
    }

    /**
     * Create captcha, add errors if exist and forward to registration page.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        req.setAttribute(ERRORS, session.getAttribute(ERRORS));
        req.setAttribute(FORM, session.getAttribute(FORM));
        session.removeAttribute(ERRORS);
        session.removeAttribute(FORM);

        log.info("Creation of captcha");
        Captcha captcha = new Captcha.Builder(200, 50)
                .addText(new DefaultTextProducer(5))
                .gimp(new FishEyeRenderer())
                .addNoise()
                .addBackground(new GradiatedBackground())
                .addBorder()
                .build();

        captchaStorage.add(req, resp, captcha);
        session.setAttribute(CAPTCHA_IMAGE, captcha.getImage());
        req.getRequestDispatcher(PAGE_REGISTER_JSP).forward(req, resp);
    }

    /**
     * Read form-data and validate them and redirect to certain page.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String login = req.getParameter(LOGIN);
        String name = req.getParameter(NAME);
        String surname = req.getParameter(SURNAME);
        String email = req.getParameter(EMAIL);
        String password = req.getParameter(PASSWORD);
        String repeatPassword = req.getParameter(REPEAT_PASSWORD);
        boolean spam = ON.equals(req.getParameter(SPAM));
        String captcha = req.getParameter(CAPTCHA_INPUT);
        Captcha captchaAnswer = captchaStorage.get(req);
        Part part = req.getPart(PICTURE);

        log.info("Read form-data");
        CheckinUser checkinUser = CheckinUser.builder()
                .login(login)
                .name(name)
                .surname(surname)
                .email(email)
                .password(password)
                .repeatPassword(repeatPassword)
                .spam(spam)
                .captcha(captcha)
                .build();

        Map<String, String> errors = UserValidate.validate(checkinUser, captchaAnswer);
        User user = userService.getUser(login);

        if (nonNull(user)) {
            log.info("Add validation errors to session(user already exist)");
            errors.put(LOGIN, "User with provided login already exist");
        }

        if (errors.size() != 0) {
            log.info("Add validation errors to session");
            session.setAttribute(ERRORS, errors);
            session.setAttribute(FORM, checkinUser);
            resp.sendRedirect(REGISTER_URL);
            return;
        }

        String fileName = fileUploadService.upload(part, login, AVATAR_PATH);

        user = ConvertUtil.convert(checkinUser, fileName);

        userService.create(user);
        session.setAttribute(USER, user);

        resp.sendRedirect(INDEX_JSP);
    }
}
