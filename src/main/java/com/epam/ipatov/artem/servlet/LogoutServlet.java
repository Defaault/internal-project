package com.epam.ipatov.artem.servlet;

import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.ipatov.artem.constant.Constant.INDEX_JSP;
import static com.epam.ipatov.artem.constant.Constant.LOGOUT_URL;
import static com.epam.ipatov.artem.constant.Constant.USER;

/**
 * LogoutServlet
 */
@Log4j
@WebServlet(LOGOUT_URL)
public class LogoutServlet extends HttpServlet {

    /**
     * Remove user from session.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.removeAttribute(USER);
        log.info("User has been deleted from session");
        req.getRequestDispatcher(INDEX_JSP).forward(req, resp);
    }
}
