package com.epam.ipatov.artem.servlet;

import com.epam.ipatov.artem.entity.User;
import com.epam.ipatov.artem.entity.dto.LoginUser;
import com.epam.ipatov.artem.service.UserService;
import com.epam.ipatov.artem.util.PasswordEncoderUtil;
import com.epam.ipatov.artem.validate.UserValidate;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

import static com.epam.ipatov.artem.constant.Constant.ERRORS;
import static com.epam.ipatov.artem.constant.Constant.INDEX_JSP;
import static com.epam.ipatov.artem.constant.Constant.LOGIN;
import static com.epam.ipatov.artem.constant.Constant.LOGIN_FORM;
import static com.epam.ipatov.artem.constant.Constant.LOGIN_URL;
import static com.epam.ipatov.artem.constant.Constant.PAGE_LOGIN_JSP;
import static com.epam.ipatov.artem.constant.Constant.PASSWORD;
import static com.epam.ipatov.artem.constant.Constant.USER;
import static com.epam.ipatov.artem.constant.Constant.USER_SERVICE;
import static java.util.Objects.isNull;

/**
 * LoginServlet
 */
@Log4j
@WebServlet(LOGIN_URL)
public class LoginServlet extends HttpServlet {

    private UserService userService;

    /**
     * Init all service dependencies.
     *
     * @param config - servlet config.
     * @throws ServletException - ServletException.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        log.info("Initialization of fields");
        ServletContext context = config.getServletContext();
        userService = (UserService) context.getAttribute(USER_SERVICE);
        log.info("Fields has been initialized");
    }

    /**
     * Add errors, login form if exists and forward to login page.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        req.setAttribute(ERRORS, session.getAttribute(ERRORS));
        req.setAttribute(LOGIN_FORM, session.getAttribute(LOGIN_FORM));
        session.removeAttribute(ERRORS);
        session.removeAttribute(LOGIN_FORM);

        req.getRequestDispatcher(PAGE_LOGIN_JSP).forward(req, resp);
    }

    /**
     * Read form-data and validate them and redirect to certain page.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String login = req.getParameter(LOGIN);
        String password = req.getParameter(PASSWORD);

        LoginUser loginUser = LoginUser.builder()
                .login(login)
                .password(password)
                .build();

        Map<String, String> errors = UserValidate.validate(loginUser);
        User user = userService.getUser(login);

        if (isNull(user)) {
            log.info("Add validation errors to session(user doesn't exist)");
            errors.put(LOGIN, "User does not exist");
        } else if (!PasswordEncoderUtil.matches(password, user.getPassword())) {
            log.info("Passwords do not match");
            errors.put(PASSWORD, "Passwords do not match");
        }

        if (errors.size() != 0) {
            log.info("Add validation errors to session");
            session.setAttribute(ERRORS, errors);
            session.setAttribute(LOGIN_FORM, loginUser);
            resp.sendRedirect(LOGIN_URL);
            return;
        }

        session.setAttribute(USER, user);

        resp.sendRedirect(INDEX_JSP);
    }
}
