package com.epam.ipatov.artem.servlet;

import com.epam.ipatov.artem.constant.PaginationEnum;
import com.epam.ipatov.artem.constant.SortEnum;
import com.epam.ipatov.artem.converter.FilterDataConverter;
import com.epam.ipatov.artem.entity.Product;
import com.epam.ipatov.artem.entity.dto.FilterData;
import com.epam.ipatov.artem.service.ProductService;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.EnumSet;
import java.util.List;

import static com.epam.ipatov.artem.constant.Constant.DRINKS_URL;
import static com.epam.ipatov.artem.constant.Constant.PAGE_DRINKS_JSP;
import static com.epam.ipatov.artem.constant.Constant.PRODUCT_SERVICE;

/**
 * DrinkServlet
 */
@Log4j
@WebServlet(DRINKS_URL)
public class DrinkServlet extends HttpServlet {

    private static final String MANUFACTURER_LIST = "manufacturerList";
    private static final String MANUFACTURES = "manufactures";
    private static final String CATEGORIES = "categories";
    private static final String CATEGORY_LIST = "categoriesList";
    private static final String ORDERS_BY = "ordersBy";
    private static final String PAGINATION = "pagination";
    private static final String COUNT_OF_PAGES = "countOfPages";
    private static final String PRODUCTS = "products";

    private ProductService productService;
    private FilterDataConverter filterDataConverter;

    /**
     * Init all service dependencies.
     *
     * @param config - servlet config.
     * @throws ServletException - ServletException.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        filterDataConverter = new FilterDataConverter();
        ServletContext context = config.getServletContext();
        productService = (ProductService) context.getAttribute(PRODUCT_SERVICE);
        EnumSet<SortEnum> sortEnums = EnumSet.allOf(SortEnum.class);
        EnumSet<PaginationEnum> paginationEnums = EnumSet.allOf(PaginationEnum.class);
        context.setAttribute(ORDERS_BY, sortEnums);
        context.setAttribute(PAGINATION, paginationEnums);
    }

    /**
     * Get all products and filter them by predicate.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        FilterData data = filterDataConverter.createData(req);
        List<String> manufactures = productService.getAllManufactures();
        List<String> categories = productService.getAllCategory();
        int countOfPages = productService.getCountByPredicate(data);
        List<Product> products = productService.getAllByPredicate(data);

        req.setAttribute(CATEGORIES, categories);
        req.setAttribute(MANUFACTURES, manufactures);
        req.setAttribute(CATEGORY_LIST, data.getCategory());
        req.setAttribute(MANUFACTURER_LIST, data.getManufacturer());
        session.setAttribute(COUNT_OF_PAGES, countOfPages);
        session.setAttribute(PRODUCTS, products);

        req.getRequestDispatcher(PAGE_DRINKS_JSP).forward(req, resp);
    }
}
