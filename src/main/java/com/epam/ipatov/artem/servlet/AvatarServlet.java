package com.epam.ipatov.artem.servlet;

import com.epam.ipatov.artem.entity.User;
import lombok.extern.log4j.Log4j;
import ml.miron.captcha.util.CaptchaServletUtil;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static com.epam.ipatov.artem.constant.Constant.AVATARS;
import static com.epam.ipatov.artem.constant.Constant.AVATAR_URL;
import static com.epam.ipatov.artem.constant.Constant.USER;

/**
 * AvatarServlet
 */
@WebServlet(AVATAR_URL)
@Log4j
public class AvatarServlet extends HttpServlet {

    /**
     * Read avatar from directory and write in response.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Read avatar from directory");
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(USER);
        BufferedImage image = ImageIO.read(new File(AVATARS + File.separator + user.getPicture()));

        CaptchaServletUtil.writeImage(resp, image);
        log.info("Write avatar to response");
    }
}
