package com.epam.ipatov.artem.servlet;

import lombok.extern.log4j.Log4j;
import ml.miron.captcha.util.CaptchaServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_IMAGE;
import static com.epam.ipatov.artem.constant.Constant.CAPTCHA_URL;

/**
 * CaptchaServlet
 */
@Log4j
@WebServlet(CAPTCHA_URL)
public class CaptchaServlet extends HttpServlet {

    /**
     * Create image of captcha and write it to response.
     *
     * @param req  - request.
     * @param resp - response.
     * @throws ServletException - ServletException.
     * @throws IOException      - IOException.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Creation of the image");
        CaptchaServletUtil.writeImage(resp, (BufferedImage) req.getSession().getAttribute(CAPTCHA_IMAGE));
        req.getSession().removeAttribute(CAPTCHA_IMAGE);
        log.info("Remove captcha image from session");
    }
}
