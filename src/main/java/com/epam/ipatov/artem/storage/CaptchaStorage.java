package com.epam.ipatov.artem.storage;

import lombok.extern.log4j.Log4j;
import ml.miron.captcha.image.Captcha;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.epam.ipatov.artem.constant.Constant.MIN_TIMEOUT;
import static com.epam.ipatov.artem.constant.Constant.TIMEOUT;

/**
 * CaptchaStorage
 */
@Log4j
public abstract class CaptchaStorage {

    protected int timeout = MIN_TIMEOUT;

    /**
     * Add captcha in chose storage.
     *
     * @param request  - request.
     * @param response - response.
     * @param captcha  - captcha.
     */
    public abstract void add(HttpServletRequest request, HttpServletResponse response, Captcha captcha);

    /**
     * Get captcha relatively chose storage.
     *
     * @param request - request.
     * @return - captcha.
     */
    public abstract Captcha get(HttpServletRequest request);

    /**
     * Init timeout, default timeout - 100 seconds.
     *
     * @param context - servlet context.
     */
    protected void initializeTimeout(ServletContext context) {
        try {
            timeout = Integer.parseInt(context.getInitParameter(TIMEOUT));
            log.info("Captcha will be terminated in " + timeout + " sec");
        } catch (NumberFormatException e) {
            log.error(e.getMessage());
        }
    }
}
