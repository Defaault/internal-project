package com.epam.ipatov.artem.storage.impl;

import com.epam.ipatov.artem.provider.KeyProvider;
import com.epam.ipatov.artem.storage.CaptchaStorage;
import lombok.extern.log4j.Log4j;
import ml.miron.captcha.image.Captcha;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * ContextCaptchaStorage
 */
@Log4j
public class ContextCaptchaStorage extends CaptchaStorage {

    private KeyProvider provider;

    public ContextCaptchaStorage(KeyProvider provider) {
        this.provider = provider;
    }

    @Override
    public void add(HttpServletRequest request, HttpServletResponse response, Captcha captcha) {
        ServletContext context = request.getServletContext();
        initializeTimeout(context);

        String uuid = UUID.randomUUID().toString();
        provider.set(request, response, uuid);
        context.setAttribute(uuid, captcha);
        log.info("Captcha has been stored");

        Executors.newSingleThreadScheduledExecutor().schedule(() -> {
            context.removeAttribute(uuid);
            log.info("Captcha has been deleted");
        }, timeout, TimeUnit.SECONDS);
    }

    @Override
    public Captcha get(HttpServletRequest request) {
        log.info("Get captcha from context");
        ServletContext context = request.getServletContext();
        return (Captcha) context.getAttribute(provider.get(request));
    }
}
