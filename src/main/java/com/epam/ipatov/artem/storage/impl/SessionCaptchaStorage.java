package com.epam.ipatov.artem.storage.impl;

import com.epam.ipatov.artem.storage.CaptchaStorage;
import lombok.extern.log4j.Log4j;
import ml.miron.captcha.image.Captcha;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.epam.ipatov.artem.constant.Constant.CAPTCHA;

/**
 * SessionCaptchaStorage
 */
@Log4j
public class SessionCaptchaStorage extends CaptchaStorage {

    @Override
    public void add(HttpServletRequest request, HttpServletResponse response, Captcha captcha) {
        ServletContext context = request.getServletContext();
        initializeTimeout(context);

        HttpSession session = request.getSession();
        session.setAttribute(CAPTCHA, captcha);
        log.info("Captcha has been stored");

        Executors.newSingleThreadScheduledExecutor().schedule(() -> {
            session.removeAttribute(CAPTCHA);
            log.info("Captcha has been deleted");
        }, timeout, TimeUnit.SECONDS);
    }

    @Override
    public Captcha get(HttpServletRequest request) {
        log.info("Get captcha from session");
        HttpSession session = request.getSession();
        return (Captcha) session.getAttribute(CAPTCHA);
    }
}
