package com.epam.ipatov.artem.tag;

import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

import static com.epam.ipatov.artem.constant.Constant.ID;

/**
 * CaptchaTag
 */
@Log4j
public class CaptchaTag extends TagSupport {

    /**
     * Load captcha image and add input field.
     */
    @Override
    public int doStartTag() throws JspException {
        JspWriter writer = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        HttpSession session = request.getSession();

        try {
            writer.print("<div id=\"captchaDiv\" class=\"form-group\">\n" +
                    "       <img id=\"captcha_img\" src=\"/captcha\"/>\n" +
                    "       <input type=\"hidden\" name=\"id\" value=" + session.getAttribute(ID) + ">\n" +
                    "       <input class=\"form-control\" name=\"captcha_input\" id=\"captcha_input\" type=\"text\" placeholder=\"\">" +
                    "</div>");
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return SKIP_BODY;
    }
}
